cmd_remake_help() {
    cat <<_EOF
    remake
        Reconstruct again the container, preserving the existing data.

_EOF
}

cmd_remake() {
    # backup
    cs users backup

    # reinstall
    cs remove
    cs make
    cs restart

    # restore
    local datestamp=$(date +%Y%m%d)
    cs users restore backup/users-$datestamp.tgz
}
