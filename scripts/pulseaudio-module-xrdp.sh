#!/bin/bash -x

# install pulseaudio-module-xrdp
# https://github.com/neutrinolabs/pulseaudio-module-xrdp/wiki/README
install_pulseaudio_module_xrdp() {
    # make sure that we have the needed tools
    apt install --yes pulseaudio build-essential libpulse-dev dpkg-dev git 

    # get the source of pulseaudio
    cat <<EOF > /etc/apt/sources.list.d/bionic-updates-src.list
deb-src http://archive.ubuntu.com/ubuntu bionic-updates main restricted universe multiverse
EOF
    apt update
    apt source pulseaudio

    # build the pulseaudio package
    apt build-dep --yes pulseaudio
    cd pulseaudio-11.1
    ./configure
    cd ..

    # build xrdp source / sink modules
    git clone https://github.com/neutrinolabs/pulseaudio-module-xrdp.git
    cd pulseaudio-module-xrdp
    ./bootstrap && ./configure PULSE_DIR=$(dirname $(pwd))/pulseaudio-11.1
    make
    make install
    cd ..

    # cleanup
    rm -rf pulseaudio*
    rm /usr/lib/pulse-11.1/modules/*.la
    rm /etc/apt/sources.list.d/bionic-updates-src.list
}

install_pulseaudio_module_xrdp
systemctl restart xrdp
