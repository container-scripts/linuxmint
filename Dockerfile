include(bionic)

### update and upgrade
RUN apt update &&\
    apt upgrade --yes

### unminimize
RUN yes | unminimize

### add LinuxMint repository
RUN apt install --yes gnupg
COPY misc/A6616109451BBBF2.pubkey /tmp/
RUN apt-key add /tmp/A6616109451BBBF2.pubkey
RUN echo "deb http://packages.linuxmint.com tricia main upstream import backport" \
    > /etc/apt/sources.list.d/linuxmint.list

### setup apt preferences
RUN echo "\
Package: *\n\
Pin: origin build.linuxmint.com\n\
Pin-Priority: 700\
" > /etc/apt/preferences.d/official-extra-repositories.pref

RUN echo "\
Package: *\n\
Pin: origin live.linuxmint.com\n\
Pin-Priority: 750\n\
\n\
Package: *\n\
Pin: release o=linuxmint,c=upstream\n\
Pin-Priority: 700\n\
\n\
Package: *\n\
Pin: release o=Ubuntu\n\
Pin-Priority: 500\n\
" > /etc/apt/preferences.d/official-package-repositories.pref

### update and upgrade
RUN apt update &&\
    apt upgrade --yes

### install the desktop and display manager
RUN DEBIAN_FRONTEND=noninteractive \
    apt install --yes -o Dpkg::Options::=--force-confnew --install-recommends \
        lightdm lightdm-settings slick-greeter \
        mint-meta-mate
RUN systemctl set-default graphical.target

### install xrdp
RUN apt install --yes \
        xrdp xorgxrdp xrdp-pulseaudio-installer && \
    sed -i /etc/xrdp/xrdp.ini \
        -e '/^\[console\]/,$ s/^/# /' \
        -e '/^\[X11rdp\]/,+8 s/^/# /' \
        -e '/^\[Xvnc\]/,+8 s/^/# /' && \
    sed -i /etc/xrdp/sesman.ini \
        -e '/^KillDisconnected/ c KillDisconnected=true' \
        -e '/^DisconnectedTimeLimit/ c DisconnectedTimeLimit=120' \
        -e '/^DisconnectedTimeLimit/ a IdleTimeLimit=250'

### install x2go server
RUN apt install --yes x2goserver fail2ban

### install xscreensaver and xautolock
RUN apt install --yes \
    xscreensaver xscreensaver-data xscreensaver-data-extra \
    xautolock

### add /usr/games on the PATH
RUN echo 'export PATH=/usr/games:$PATH' >  /etc/profile.d/path.sh

RUN apt purge --yes flatpak && \
    apt autoremove --yes

### install epoptes
RUN apt install --yes epoptes && \
    sed -i /etc/default/epoptes-client \
        -e 's/^WOL/#WOL/' \
        -e 's/^#SERVER.*/SERVER=localhost/' &&\
    systemctl enable epoptes

### copy the script that manages user accounts
COPY scripts/users.sh /usr/local/bin/users.sh

### install additional packages
sinclude(packages)