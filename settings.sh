APP="linuxmint"

### Docker settings.
IMAGE="conscr/linuxmint:19.3"
#IMAGE="conscr/linuxmint:19.3-minimal"
#IMAGE="conscr/linuxmint:19.3-edu"
CONTAINER="linuxmint-1"

### Forwarded ports
#X2GO_PORT="2202"
#PORTS="$X2GO_PORT:22"

### Epoptes admins. Uncomment to enable.
#EPOPTES_USERS="user1 user2"

### Admin account. Uncomment to enable.
#ADMIN_USER="admin"
#ADMIN_PASS="pass"

### Guest account. Uncomment to enable.
### This account will be used as a template for guest accounts.
#GUEST_USER="guest"
#GUEST_PASS="pass"

### SMTP server for sending notifications. You can build an SMTP server
### as described here:
### https://gitlab.com/container-scripts/postfix/blob/master/INSTALL.md
### Comment out if you don't have a SMTP server and want to use
### a gmail account (as described below).
#SMTP_SERVER="smtp.example.org"
#SMTP_DOMAIN="example.org"

### Gmail account for notifications. This will be used by ssmtp.
### You need to create an application specific password for your account:
### https://www.lifewire.com/get-a-password-to-access-gmail-by-pop-imap-2-1171882
#GMAIL_ADDRESS=username@gmail.org
#GMAIL_PASSWD=hdfhfdjkfglk

