# Linux Mint MATE in a Container

## Installation

### Making The Container

- First install `cs`:
  https://gitlab.com/container-scripts/cs#installation

- Then get the scripts: `cs pull linuxmint`

- Create a dir for the container: `cs init linuxmint @linuxmint-1`

- Fix the settings: `cd /var/cs/linuxmint-1/; vim settings.sh`

- Make the container: `cs make`

See also this interactive tutorial:
https://katacoda.com/dashohoxha/courses/container-scripts/virtual-computer-lab

### Installing Additional Packages

You can go inside the docker container and install other packages with
`apt`, like this:

```bash
cd /var/cs/linuxmint-1/
cs shell
apt install ubuntu-edu-preschool ubuntu-edu-primary firefox
exit
```

However, in case you rebuild the server (for example with `cs make` or
`cs remake`), you will have to install them again manually. To install
them automatically on each rebuild, create a file like
`/var/cs/linuxmint-1/packages` with a content like this:

```
RUN DEBIAN_FRONTEND=noninteractive \
    apt install --yes \
        ubuntu-edu-preschool \
        ubuntu-edu-primary \
        ubuntu-edu-secondary
```

**Note:** See also
[packages.sample](https://gitlab.com/container-scripts/linuxmint/blob/master/packages.sample).

Then change the name of the `IMAGE` on `seetings.sh` and run:

```bash
cs build
cs make
```

This will build a local image and use that one for the container. Now
whenever you do `cs make` or `cs remake` the local image will be used.

**Note:** To rebuild the image from scratch you can use `cs build
--no-cache`. This will take longer, but sometimes it is needed (if
the docker cache is outdated).

## Usage

### Accessing With X2Go

In order to access this server with
[X2Go](https://wiki.x2go.org/doku.php/doc:installation:x2goclient),
you should uncomment these lines on `seetings.sh`, before `cs make`:

```bash
### Forwarded ports
X2GO_PORT="2202"
PORTS="$X2GO_PORT:22"
```

This will allow port `2202` on the host to be forwarded to port `22`
on the container. On the X2Go client you should use the port `2202`.

You can change the port `2202` in `settings.sh` to something else (for
example `22022`).

**Note:** Make sure to allow the port `2202` (or `22022`) on the
firewall (for example with: `ufw allow 2202`).

**Note:** You can also access the server from terminal, like this:
```bash
ssh -p 2202 user1@123.45.67.89
```

### Accessing From Guacamole

First install it: https://gitlab.com/container-scripts/guacamole#installation

Then add a Guacamole user and connection for accessing this server:

```bash
cd /var/cs/guac.example.org/
cs guac user add user1 pass1
cs guac server add linuxmint-1
cs guac user connect user1 linuxmint-1
```

Here `linuxmint-1` is the name of the container (the option
`CONTAINER=linuxmint-1` on `settings.sh`).

### Managing User Accounts

The file `accounts.txt` contains a list of accounts in the form
`username:password`, which are created automatically when the
container is created. But they can also be re-created any time with a
command like this:

```bash
cs users create accounts.txt
```

There are other commands and options which can be used to **export**,
**import**, **backup** and **restore** user accounts, for example:

```bash
cs users backup
cs users restore backup/users-20190417.tgz
```

See also: `cs inject users.sh help`

### Epoptes Users

To allow one or more users to open Epoptes and watch the other users
(usually these are the teachers), add them to the option
`EPOPTES_USERS` on `settings.sh`. This has to be done before `cs
make`.

You can also add them manually like this:

```bash
cs exec adduser user1 epoptes
```

However this is not persistent (after a `cs make` you will have to add
them manually again).

### Admin Users

An admin user can install/uninstall packages with `sudo apt ...`.

He can also create/backup/restore user accounts with `sudo users.sh ...`.

To add an admin user, uncomment and modify these lines on
`settings.sh`:

```bash
## Admin account. Uncomment to enable.
ADMIN_USER="admin"
ADMIN_PASS="pass1"
```

You can also add an admin user manually, like this:

```bash
cs inject add-admin.sh user1 pass1
```

However this is not persistent (after a `cs make` you will have to add
it manually again).

### Other Topics

- [Guest Accounts](https://gitlab.com/container-scripts/linuxmint/-/wikis/Guest-Accounts)

### Other Commands

```
cs help
cs stop
cs start
cs shell
cs remake
```
