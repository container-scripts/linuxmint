cmd_config_help() {
    cat <<_EOF
    config
        Run configuration scripts inside the container.

_EOF
}

cmd_config() {
    check_settings

    cs inject ubuntu-fixes.sh
    cs inject ssmtp.sh
    cs inject logwatch.sh
    
    # create and setup user accounts
    cs inject setup-user-accounts.sh
    [[ -f accounts.txt ]] || cp $APP_DIR/misc/accounts.txt .
	cs users create accounts.txt

    # add an admin user
    [[ -n $ADMIN_USER ]] && \
        cs inject add-admin.sh $ADMIN_USER $ADMIN_PASS

    # add users to the group 'epoptes'
    for user in $EPOPTES_USERS; do
        cs exec adduser $user epoptes
    done

    #cs inject pulseaudio-module-xrdp.sh
}

fail() { echo "$@" >&2; exit 1; }

check_settings() {
    [[ $ADMIN_PASS == 'pass' ]] \
	&& fail "Error: ADMIN_PASS on 'settings.sh' has to be changed for security reasons."

    [[ $GUEST_PASS == 'pass' ]] \
	&& fail "Error: GUEST_PASS on 'settings.sh' has to be changed for security reasons."
}
