cmd_users_help() {
    cat <<_EOF
    users [<command>] [<filename>]
        Run 'cs inject users.sh [<command>] [<filename>]'
        Where <command> can be: 
            help | create | create-guests | remove-guests
            | export | import | backup | restore

_EOF
}

cmd_users() {
    set -x
    cs inject users.sh "$@"
}
